import { AppPage } from './app.po';
import { Utils } from './utils.po';
import { browser, logging, protractor, Key } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should contain a word frequency analyzer', () => {
    page.navigateTo();
    expect(page.getWordFrequencyAnalyzerEl()).toBeTruthy();
  });
  
  it('should analyze the words whenever text is inputted in the textarea', async (done) => {
    page.navigateTo();
    var EC = protractor.ExpectedConditions;
    let textareaEl = page.getAnalyzeTextareaEl();
    let topOccurringWordsInputEl = page.getTopOccurringInputEl();    
    let topOccurringWordsEl = page.getTopOccurringWordsEl();

    expect(textareaEl).toBeTruthy();
    // just test the top occurring words list. If that works, the analyzing part works as well.
    expect(topOccurringWordsInputEl).toBeTruthy();
    expect(topOccurringWordsEl).toBeTruthy();

    let verySpecificWord = 'veryspecificwordASDLKJQOIDAS'.toLowerCase(); // should always be lowercase, so enforcing that this way
    let repeatTimes = 4; 
    let testingStr = Utils.repeat(verySpecificWord, repeatTimes);

    // to check this correctly and reduce fragility, we have to also set the top occurrence input to a high value
    await Utils.replaceText(topOccurringWordsInputEl, '100');

    await Utils.replaceText(textareaEl, testingStr);

    // this is a pretty shallow test because all other functionality is being handled by more efficient unit and integration tests. 
    // Simply check if the list contains the word and the times it has been repeated. If it does, we can safely assume that the text has been analyzed correctly
    await browser.wait(EC.textToBePresentInElement(topOccurringWordsEl, verySpecificWord), 5000);
    let topOccuringWordsText = topOccurringWordsEl.getText();
    expect(topOccuringWordsText).toContain(repeatTimes.toString());
    done();
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
