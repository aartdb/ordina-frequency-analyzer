import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getWordFrequencyAnalyzerEl() {
    return element(by.css('[data-e2e-id="word-frequency-analyzer"]'));
  }

  getAnalyzeTextareaEl() {
    return this.getElByCss('[data-e2e-id="analyze-input"]');
  }

  getTopOccurringInputEl() {
    return this.getElByCss('[data-e2e-id="top-occurrence-words-input"]');    
  }

  getTopOccurringWordsEl() {
    return this.getElByCss('[data-e2e-id="top-occurrence-words-list"]');
  }

  private getElByCss(selector:string) {
    let parentEl = this.getWordFrequencyAnalyzerEl();
    return parentEl ? parentEl.element(by.css(selector)) : null;
  }
}
