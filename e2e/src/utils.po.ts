import { browser, Key } from 'protractor';

export class Utils {
  /**
   * Repeat a string with spaces around it
   * @example repeat('word', 3);
   * // returns ' word  word '
   */
  static repeat(str, n) {
    return (' ' + str + ' ').repeat(n);
  }

  static replaceText(el:any, newValue:string) {
    let promise =  browser
      .actions()
        .click(el)
        // select it all
        .sendKeys(Key.chord(Key.CONTROL, "a"))
        // input the verySpecificStr
        .sendKeys(newValue)
        .perform();

    return promise;
  }
}