import { async, ComponentFixture, TestBed } from'@angular/core/testing';

import { WordFrequencyAnalyzerComponent } from'./word-frequency-analyzer.component';
import { FormsModule } from'@angular/forms';
import { WordFrequency } from'./word-frequency';
import { Component, DebugElement } from'@angular/core';
import { WordFrequencyAnalyzer } from './word-frequency-analyzer';
import { TestingUtilsService } from '../services/testing-utils.service';

describe('WordFrequencyAnalyzerComponent', () => {
  let testHostComponent: TestHostComponent;
  let testHostFixture: ComponentFixture<TestHostComponent>;
  let component: WordFrequencyAnalyzerComponent;
  let fixture: ComponentFixture<WordFrequencyAnalyzerComponent>;
  let ne:HTMLHtmlElement;

  // default string to test if it shallowly works
  const defaultTestingStr = "testing string";

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordFrequencyAnalyzerComponent, TestHostComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));
 
  beforeEach(() => {
    testHostFixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = testHostFixture.componentInstance;
    testHostFixture.detectChanges();

    fixture = TestBed.createComponent(WordFrequencyAnalyzerComponent);
    component = fixture.componentInstance;
    ne = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should have a prop component.wordFrequencyAnalyzer which is an instance of WordFrequencyAnalyzer', () => {
    expect(component.wordFrequencyAnalyzer instanceof WordFrequencyAnalyzer)
      .toBe(true);
  });

  it('should make component.getMostFrequentNWords return the same as WordFrequencyAnalyzer.calculateMostFrequentNWords', () => {
    let testingStr = 'A random testing string, string, string';
    let wordToTest = 'string';
    component.text = testingStr;
    
    expect(component.getFrequencyForWord(wordToTest))
      .toEqual(component.wordFrequencyAnalyzer.calculateFrequencyForWord(testingStr, wordToTest))
  });

  it('should make component.getMostFrequentNWords return the same as WordFrequencyAnalyzer.calculateMostFrequentNWords', () => {
    let n = 4;
    component.text = defaultTestingStr;
    
    expect(component.getMostFrequentNWords(n))
      .toEqual(component.wordFrequencyAnalyzer.calculateMostFrequentNWords(defaultTestingStr, n))
  });
  
  /****************************************************************************
   * Test host tests to see if the input attributes work correctly
   * These need to be async because otherwise the value isn't yet updated
   ****************************************************************************/
  it('should display the inputted text via the `text` attr', async(() => {
    let testElSelector ='[data-test-id="has-text-prop"] [data-test-id="analyze-input"]';

    testHostFixture.whenStable().then(() => {
      let de:HTMLTextAreaElement = testHostFixture.nativeElement.querySelector(testElSelector);
      expect(de.value).toBe(defaultTestingStr);
    });
  }));

  // needs to be async to be sure that the value doesn't change
  it('should display the default text when nothing is entered in the `text` attr', async(() => {
    let testElSelector ='[data-test-id="no-text-prop"] [data-test-id="analyze-input"]';

    testHostFixture.whenStable().then(() => {
      let de:HTMLTextAreaElement = testHostFixture.nativeElement.querySelector(testElSelector);
      let defaultText = component.text; // just the default text, doesn't matter what host called it
      
      expect(de.value).toBe(defaultText);
    });
  }));

  /****************************************************************************
   * Normal integration tests
   ****************************************************************************/
  it('should update the component text prop when different text is inputted in the textarea', async(() => {
    let testElSelector ='[data-test-id="analyze-input"]';
    let de:HTMLTextAreaElement = ne.querySelector(testElSelector);
    let newTestString ='new test string, pretty unique';

    // simulate input event to trigger ngModelChange
    changeValue(de, newTestString);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.text).toBe(newTestString);
    })
  }));

  it('should debounce the text that was entered into the text field', async(() => {
    let testElSelector ='[data-test-id="analyze-input"]';
    let de:HTMLTextAreaElement = ne.querySelector(testElSelector);
    let newTestString ='new test string, pretty unique';

    // simulate input event to trigger ngModelChange
    changeValue(de, newTestString);
    fixture.detectChanges();

    // at first, it should not be set to newTestString, because it's being debounced for performance reasons
    expect(component.text).not.toBe(newTestString);
    
    fixture.whenStable().then(() => {
      // and NOW it should be the set to newTestString
      expect(component.text).toBe(newTestString);
    });
  }));

  it('should analyze the words whenever text is inputted in the textarea', async(() => {
    let testElSelector ='[data-test-id="analyze-input"]';
    let de:HTMLTextAreaElement = ne.querySelector(testElSelector);
    let repeatTimes = 5;
    let testWord = 'veryuniquewordjusttotest';
    let testingStr = TestingUtilsService.repeat(testWord, repeatTimes);

    // simulate input event to trigger ngModelChange
    changeValue(de, testingStr);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.getFrequencyForWord(testWord)).toBe(repeatTimes); // just call any 'analyze'-method. If this returns something else, it hasn't worked
    }); 
  }));

  it('should display the correct amount of the inputted word', () => {
    let testElSelectorInput ='[data-test-id="occurrence-word-input"]';
    let testElSelector ='[data-test-id="occurrence-word-output"]';

    let testStr ='the';
    let repeatTimes = 6;
    component.text = `
      dummy dummy 
      ${TestingUtilsService.repeat(testStr, repeatTimes)}
    `;
    changeValue(ne.querySelector(testElSelectorInput), testStr);
    fixture.detectChanges();

    expect(ne.querySelector(testElSelector).innerHTML).toContain(repeatTimes.toString());
  });

  it('should display the word and frequency of the correct amount of words when a different number is inputted for top N occurring words', () => {
    let testElSelectorInput ='[data-test-id="top-occurrence-words-input"]';
    let testElSelector:any ='[data-test-id="top-occurrence-words-list"]';
    let testEl = ne.querySelector(testElSelector);

    // make sure 'aart' is displayed at its right spot, so below 3 (to be displayed after 'the')
    let testStrs = [{
      str: 'teststr',
      times: 8
    },{
      str: 'aart',
      times: 2
    }];

    component.text = `
      ${TestingUtilsService.repeat(testStrs[0].str, testStrs[0].times)} 
      the the the
      ${TestingUtilsService.repeat(testStrs[1].str, testStrs[1].times)} 
      foo
    `;

    changeValue(ne.querySelector(testElSelectorInput), '3');
    fixture.detectChanges();
    
    let testListItems = testEl.querySelectorAll('[data-test-id="top-occurrence-words-list__item"]');
    expect(testListItems.length).toBe(3);

    // and a quick check
    let item1Content = testListItems[0].innerHTML.toLowerCase();
    expect(item1Content).toContain(testStrs[0].str);
    expect(item1Content).toContain(testStrs[0].times.toString());

    let item4Content = testListItems[2].innerHTML.toLowerCase();
    expect(item4Content).toContain(testStrs[1].str);
    expect(item4Content).toContain(testStrs[1].times.toString());

    // just to be sure:'foo'should not be displayed
    expect(testEl.innerHTML).not.toContain('foo');
  });

  /**
   * Change the value so it's recognized by Jasmine
   * @param de debugElement, any HTML element
   * @param newValue the new value to set
   */
  let changeValue = (de, newValue:string):void => {
    de.value = newValue;
    var event = new Event('input', {
     'bubbles': true,
     'cancelable': true
    });
    de.dispatchEvent(event);
  }

  @Component({
    selector:'test-host-component',
    template: `
      <word-frequency-analyzer data-test-id="has-text-prop" [text]="'${defaultTestingStr}'"></word-frequency-analyzer>
      <word-frequency-analyzer data-test-id="no-text-prop"></word-frequency-analyzer>
    ` 
  })
  class TestHostComponent { }
});
