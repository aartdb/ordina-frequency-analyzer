import { Component, Input, OnInit } from '@angular/core';
import { WordFrequency } from '../word-frequency-analyzer/word-frequency';
import { WordFrequencyAnalyzer } from './word-frequency-analyzer';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { IWordCountMap } from '../interfaces/IWordCountMap';

@Component({
  selector: 'word-frequency-analyzer',
  templateUrl: './word-frequency-analyzer.component.html',
  styleUrls: ['./word-frequency-analyzer.component.scss'],
  providers: [ WordFrequencyAnalyzer ]
})
export class WordFrequencyAnalyzerComponent implements OnInit {
  @Input('text') text = 'standaard tekst';
  textModel:string;
  textChanged$:Subject<string> = new Subject<string>();

  mostFrequentWordsMap:IWordCountMap = {};
  wordsMap:IWordCountMap = {};

  inputFreqWord = 'woord';
  inputTopFreqWords = 2;

  constructor(public wordFrequencyAnalyzer: WordFrequencyAnalyzer) { 
    this.textChanged$
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe(text => this.text = text);
  }

  ngOnInit() {
    this.textModel = this.text; // textModel is being debounced for performance reasons
  }

  textModelChanged(newText:string):void {
    this.textChanged$.next(newText); 
  }
  
  getMostFrequentNWords(numberOfWords:number):WordFrequency[] {
    return this.wordFrequencyAnalyzer.calculateMostFrequentNWords(this.text, numberOfWords);
  } 
s
  getFrequencyForWord(word:string):number {
    return this.wordFrequencyAnalyzer.calculateFrequencyForWord(this.text, word);
  }
}
