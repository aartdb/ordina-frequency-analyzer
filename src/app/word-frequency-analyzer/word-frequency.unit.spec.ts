import { WordFrequency } from "./word-frequency"

describe('WordFrequency', () => {
    let defaultTestWord:WordFrequency;

    beforeEach(() => {
        defaultTestWord = new WordFrequency('defaultTestWord', 6);
    });

    it('should correctly set the word to the word prop', () => {
        let testWord = 'word';
        let word = new WordFrequency(testWord, 5);

        expect(word.word).toBe(testWord);
    });

    it('should correctly set the frequency to the frequency prop', () => {
        let testFrequency = 2;
        let word = new WordFrequency('dummy', testFrequency);

        expect(word.frequency).toBe(testFrequency);
    });
    
    it('should set the frequency to 0 when a negative value is entered in the frequency prop', () => {
        let word = new WordFrequency('test', 5);

        word.frequency = -1;

        expect(word.frequency).toBe(0);
    });
    
    it('should set the frequency to 2 when 2 is entered in the frequency prop', () => {
        let word = new WordFrequency('test', 0);
        let testFrequency = 2;
        
        word.frequency = testFrequency;

        expect(word.frequency).toBe(testFrequency);
    });

    it('should alias `get word` to getWord()', () => {
        expect(defaultTestWord.getWord()).toEqual(defaultTestWord.word);
    });

    it('should alias `get frequency` to getFrequency()', () => {
        expect(defaultTestWord.getFrequency()).toEqual(defaultTestWord.frequency);
    });
})