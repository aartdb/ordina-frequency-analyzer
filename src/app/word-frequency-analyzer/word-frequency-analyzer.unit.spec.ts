import { WordFrequencyAnalyzer } from "./word-frequency-analyzer";
import { WordFrequency } from './word-frequency';
import { TestingUtilsService } from '../services/testing-utils.service';
import { IWordCountMap } from '../interfaces/IWordCountMap';

describe('WordFrequencyAnalyzer', () => {
    let wordFrequencyAnalyzer:WordFrequencyAnalyzer;
    let generalTestingStr = 'word word word foo foo bar';

    beforeEach(() => {
        wordFrequencyAnalyzer = new WordFrequencyAnalyzer();
    });

    
  it('should analyze the words case-insensitively', () => {
    let wordCountMap:IWordCountMap = wordFrequencyAnalyzer.parseToCountMap('Test TEST test TEst');

    expect(wordCountMap.test).toBe(4);
  });

  it('should be able to separate words by different separators (space, comma, dash)', () => {
    let wordCountMap:IWordCountMap = wordFrequencyAnalyzer.parseToCountMap('the,test test-foo');

    expect(wordCountMap.the).toBe(1);
    expect(wordCountMap.test).toBe(2);
    expect(wordCountMap.foo).toBe(1);
  });
    
    it('should calculate the correct frequency for a given word', () => {
        let repeatTimes = 3;
        let testingStr = TestingUtilsService.repeat('word', repeatTimes) + TestingUtilsService.repeat('foo', repeatTimes) + 'bar';
        
        let wordFrequency = wordFrequencyAnalyzer.calculateFrequencyForWord(testingStr, 'word');

        expect(wordFrequency).toBe(repeatTimes);
        
        // second test, check for a word that wasn't entered in the text
        wordFrequency = wordFrequencyAnalyzer.calculateFrequencyForWord(testingStr, 'nonExistingWord');

        expect(wordFrequency).toBe(0);
    });

    it('should calculate the most frequent N words correctly and sort them from low to high', () => {
        let testingStr = 
            TestingUtilsService.repeat('foo', 5) + 
            TestingUtilsService.repeat('word', 8) + 
            TestingUtilsService.repeat('test', 3) +
            TestingUtilsService.repeat('bar', 4);
        let n = 4;

        let mostFrequentNWords = wordFrequencyAnalyzer.calculateMostFrequentNWords(testingStr, n);

        expect(mostFrequentNWords.length).toBe(n);
        expect(mostFrequentNWords).toEqual([
            new WordFrequency('word', 8),
            new WordFrequency('foo', 5),
            new WordFrequency('bar', 4),
            new WordFrequency('test', 3)
        ]);
    });

    it('should sort the words alphabetically when they have the same frequency', () => {
        let repeatTimes = 5;
        let testingStr = 
            TestingUtilsService.repeat('zzz', repeatTimes) + 
            TestingUtilsService.repeat('ccc', repeatTimes) + 
            TestingUtilsService.repeat('bbb', repeatTimes) +
            TestingUtilsService.repeat('aaa', repeatTimes);

        let mostFrequentNWords = wordFrequencyAnalyzer.calculateMostFrequentNWords(testingStr, 100);

        expect(mostFrequentNWords).toEqual([
            new WordFrequency('aaa', repeatTimes),
            new WordFrequency('bbb', repeatTimes),
            new WordFrequency('ccc', repeatTimes),
            new WordFrequency('zzz', repeatTimes)
        ]);
    });

    it('should not return more than the amount of entered unique when calling .calculateMostFrequentNWords', () => {
        let testingStr = 
            TestingUtilsService.repeat('foo', 5) + 
            TestingUtilsService.repeat('word', 8) + 
            TestingUtilsService.repeat('test', 3) +
            TestingUtilsService.repeat('bar', 4);
        let n = 500;

        let mostFrequentNWords = wordFrequencyAnalyzer.calculateMostFrequentNWords(testingStr, n);

        expect(mostFrequentNWords.length).toBe(4);
    });

    it('should parse the count map with the correct words and their correct frequency', () => {
        let testStr = TestingUtilsService.repeat('word', 3) +
                TestingUtilsService.repeat('the', 2) +
                TestingUtilsService.repeat('foo', 5);

        let countMap:IWordCountMap = wordFrequencyAnalyzer.parseToCountMap(testStr);

        expect(countMap.word).toBe(3);
        expect(countMap.the).toBe(2);
        expect(countMap.foo).toBe(5);
    });
});

describe('WordFrequencyAnalyzer.calculateHighestFrequency', () => {
    let wordFrequencyAnalyzer:WordFrequencyAnalyzer;

    beforeEach(() => {
        wordFrequencyAnalyzer = new WordFrequencyAnalyzer();
    });

    it('should return highest frequency of the words correctly', () => {
        let repeatTimes = 3;
        let testingStr = 
            TestingUtilsService.repeat('word', repeatTimes) + 
            'foo foo bar';
        let highestFrequency = wordFrequencyAnalyzer.calculateHighestFrequency(testingStr);

        expect(highestFrequency).toBe(repeatTimes);
    });

    it('should caclulate the right frequency when two words have the same frequency', () => {
        let repeatTimes = 3;
        let testingStr = 
            TestingUtilsService.repeat('word', repeatTimes) + 
            TestingUtilsService.repeat('foo', repeatTimes) + 
            'bar';
        let highestFrequency = wordFrequencyAnalyzer.calculateHighestFrequency(testingStr);

        expect(highestFrequency).toBe(repeatTimes);
    });
    
    it('should return 0 when calculating the highest frequency with an empty string', () => {
        let testingStr = '';
        let highestFrequency = wordFrequencyAnalyzer.calculateHighestFrequency(testingStr);

        expect(highestFrequency).toBe(0);
    });
});