import { WordFrequency } from './word-frequency';
import { IWordCountMap } from '../interfaces/IWordCountMap';
import { IWordFrequencyAnalyzer } from '../interfaces/IWordFrequencyAnalyzer';

export class WordFrequencyAnalyzer implements IWordFrequencyAnalyzer {
  /**
   * Get the frequency of the most occurring word(s)
   */
  calculateHighestFrequency(text:string):number {
    let highest = this.calculateMostFrequentNWords(text, 1)[0];
    return highest ? highest.frequency : 0;
  } 

  /**
   * Get the frequency for the provided word
   * @param {string} text haystack
   * @param {string} word needle
   * @returns {number} the frequency
   * @example calculateFrequencyForWord('word word word random dummy', 'word')
   * // returns 3
   */
  calculateFrequencyForWord(text:string, word:string):number {
    return this.parseToCountMap(text)[word] || 0;
  }

  /**
   * Get the most frequent number of words. I.e.: 2 returns an array of the two most frequent words
   */
  calculateMostFrequentNWords(text:string, n:number):WordFrequency[] {
    return this.parseText(text)
        .sort( (a, b) => {
          if(b.frequency !== a.frequency) {
            return b.frequency - a.frequency
          }
          return b.word < a.word ? 1 : -1; // if frequency is the same, sort alphabetically
        })
        .slice(0, n);
  }

  /**
   * Get the occurrence of words in a map
   */
  parseToCountMap(text:string):IWordCountMap {
    let words = text
        .split(/[\s,-]+/)
        .map(w => w.trim().toLowerCase())
        .filter(w => w); // remove empty words

    let wordCountMap:IWordCountMap = {};
    words.forEach(w => {
        wordCountMap[w] = (wordCountMap[w] || 0) + 1; // start at 1 because it should be human readable
    });
    return wordCountMap;
  }

  /**
   * Parse the text into an array of WordFrequency objects
   */
  private parseText(text:string):WordFrequency[] {
      let wordCountMap = this.parseToCountMap(text);
      return Object.keys(wordCountMap)
          .map(key => new WordFrequency(key, wordCountMap[key]));
  }
}