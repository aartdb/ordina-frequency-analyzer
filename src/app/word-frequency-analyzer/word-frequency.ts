import { IWordFrequency } from '../interfaces/IWordFrequency';

export class WordFrequency implements IWordFrequency {
    constructor(private _word:string, private _frequency:number) { }

    // I highly prefer getters and setters for properties over 'getProp'-methods, so I added those as well
    get word() {
      return this._word;
    }
    getWord() { // alias of .word
      return this.word;
    }

    get frequency() {
      return this._frequency;
    }
    getFrequency() { // alias of .frequency
      return this.frequency;
    }
  
    set frequency(frequency) {
      this._frequency = frequency < 0 ? 0 : frequency;
    }
  }
  