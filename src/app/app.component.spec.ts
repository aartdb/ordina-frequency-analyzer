import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule, By } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { WordFrequencyAnalyzerComponent } from './word-frequency-analyzer/word-frequency-analyzer.component';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        WordFrequencyAnalyzerComponent
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges;
  });
  

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should have a word frequency analyzer', () => {
    let waComponent = de.query(By.directive(WordFrequencyAnalyzerComponent));
    expect(waComponent).not.toBeNull();
  });
});
