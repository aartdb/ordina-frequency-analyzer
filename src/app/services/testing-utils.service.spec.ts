import { TestBed } from '@angular/core/testing';

import { TestingUtilsService } from './testing-utils.service';

describe('TestingUtilsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ TestingUtilsService ]
    }).compileComponents();

  });

  it('should be created', () => {
    expect(TestingUtilsService).toBeTruthy();
  });

  it('should put spaces around the word', () => {
    let testStr = TestingUtilsService.repeat('word', 2);

    // generic check
    expect(testStr).toContain('word');
    expect(testStr).toContain(' ');
  })

  it('should repeat the word the correct amount of times with a positive value', () => {
    let repeatTimes = 5;
    let testStr = TestingUtilsService.repeat('word', repeatTimes);

    expect(testStr.match(/word/g).length).toBe(repeatTimes);
  });

  it(`should repeat the word 1 time when it's asked to repeat it 0 times`, () => {
    let repeatTimes = 0;
    let testStr = TestingUtilsService.repeat('word', repeatTimes);

    expect(testStr.match(/word/g).length).toBe(1);
  });

  it(`should repeat the word 1 time when it's asked to repeat it a negative amount of times`, () => {
    let repeatTimes = -5;
    let testStr = TestingUtilsService.repeat('word', repeatTimes);

    expect(testStr.match(/word/g).length).toBe(1);
  });
});
