import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TestingUtilsService {

  /**
   * Repeat a string with spaces around it so it gets recognized as separate words
   * @example repeat('word', 3);
   * // returns ' word  word '
   */
  static repeat(str, n) {
    if(n <= 0) n = 1;
    return (' ' + str + ' ').repeat(n);
  }
}
