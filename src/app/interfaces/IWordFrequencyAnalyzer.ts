import { WordFrequency } from 'src/app/word-frequency-analyzer/word-frequency';

export interface IWordFrequencyAnalyzer {
    calculateHighestFrequency(text:string):number;
    calculateFrequencyForWord (text:string, word:string):number;
    calculateMostFrequentNWords(text:string, n:number):WordFrequency[];
}
  